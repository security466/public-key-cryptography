#!/bin/bash
all: compile mkalias

#For compile:
compile:
	javac -cp ".:./getopt.jar" RSA.java

#Make alias:
mkalias:
	alias RSA="java \-cp \".:./getopt.jar\" RSA"

clean:
	rm -f *.class